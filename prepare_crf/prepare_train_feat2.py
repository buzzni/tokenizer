#coding:utf-8
import tokenizer.dic_utils as du
import tokenizer.tokenize_utils as tu

def tagging(txt, mode="train"):
    u_list = list(txt)
    result = []
    tags = get_tags(txt)

    for tag2, word, next_word in zip(tags, u_list, u_list[1:] + [" "]):
        tag = 'W'

        if word == ' ':
            continue
        if next_word == ' ':
            tag = 'S'

        if mode == 'train':
            result.append("".join([word.encode('utf8'), "\t", tag2, "\t", tag]))
        else:
            result.append("".join([word.encode('utf8'), "\t", tag2]))

    return result

def get_tags(line):
    tags = []
    toks = line.split(' ')
    for tok in toks:
        tag = "UK"
        if du.is_seller(tok):
            tag = "SL"
        elif du.is_brand(tok):
            tag = "BR"
        elif du.is_cate(tok):
            tag = "CA"
        tags.extend([tag] * len(tu.decode(tok)))
        tags.append("SP")  # space
    tags.pop()
    return tags

def convert_to_train(fname, fout_name, mode="train"):
    idx = 0
    with file(fout_name,"w") as fout:
        with file(fname) as fin:
            for line in fin.xreadlines():
                line = line.strip().decode('utf-8')
                items = tagging(line, mode=mode)
                for item in items:
                    print >> fout, item
                print >> fout, ""
                idx += 1
                if idx%10000 == 0:
                    print idx


# test_str = '1300K 메탈 테라스 잡지 꽂이 나비 L'.decode('utf-8')
# tags = get_tags(test_str)
# print tags
# print len(tags), len(list(test_str))

# u_list = list('안녕하세요 마틴 틴틴'.decode('utf8'))
# for pre_word, word, next_word in zip([" "] + u_list[:-1], u_list, u_list[1:] + [" "]):
#     tag = 'W'
#
#     if pre_word == ' ':
#         tag2 = 'B'
#     else :
#         tag2 = 'I'
#
#     if word == ' ':
#         continue
#     if next_word == ' ':
#         tag = 'S'
#
#
#     print word, tag2, tag


# if __name__ == "__main__":
#     import sys
#     convert_to_train(sys.argv[1],sys.argv[2])
#
#     fname = sys.argv[2]
#     data_list = []
#     with file(fname) as fin:
#         for line in fin.xreadlines():
#             data_list.append(line.strip())
#
#     train_size = int(len(data_list)*0.9)
#
#     with file(fname+".train","w") as fout:
#         for line in data_list[:train_size]:
#             print >> fout, line
#
#     with file(fname + ".test", "w") as fout:
#         for line in data_list[train_size:]:
#             print >> fout, line

# fin = '/Users/martin/workspace/tokenizer/eval/crf_model/data/concat_seg_seg.txt'
# fout = '/Users/martin/workspace/tokenizer/eval/crf_model/data/concat_seg_seg.feat2'
fin = '/Users/martin/workspace/tokenizer/eval/eval_test4crf.txt'
fout = '/Users/martin/workspace/tokenizer/eval/eval_test4crf_refined.txt'
convert_to_train(fin, fout, mode="test")