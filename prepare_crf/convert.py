import codecs
fnm = '/Users/martin/workspace/tokenizer/eval/crf_model/feat2.out'
outnm = '/Users/martin/workspace/tokenizer/eval/eval_test.txt'

with codecs.open(fnm, 'rt') as fin, codecs.open(outnm, 'w') as fout:
    c_line = ""
    for line in fin:
        line = line.strip()
        if line == "" :
            # print c_line
            print >> fout, c_line.strip()
            c_line = ""

        items = line.split("\t")
        c_line += items[0]

        if items[-1] == "S":
            c_line += " "