# -*- coding: utf-8 -*-
from sklearn.externals import joblib
from common import encode
from config import ENV_PATH
# from build_brand_dic import brand_dic

PARENT = ENV_PATH + 'dic/'
BRAND_DIC_PATH = PARENT + 'brand_dic.dump'
# BRAND_DIC_PATH = '/Users/martin/workspace/datalabs/nlp/dict/brand.dict'
CATE_DIC_PATH = PARENT + "cate.dict"
PRD_PATH = PARENT + "prd.dict"

BRAND = joblib.load(BRAND_DIC_PATH)
# brand_dic = joblib.load('/Users/martin/workspace/datalabs/nlp/dict/brand.dict')
CATE = joblib.load(CATE_DIC_PATH)
SELLER = ['바보사랑', '아이반찬', '보리보리', '패션플러스', '1300k', '1200m']
# SELLER = joblib.load('dic/seller.dict')

FRQ = joblib.load(PRD_PATH)
FRQ2 = joblib.load(PARENT + "word_ct_dict.dat")


def is_cate(word):
    return encode(word) in CATE

def is_brand(word):
    return encode(word) in BRAND

def is_seller(word):
    return encode(word.lower()) in SELLER \
           # and seller_dic[encode(word)] > 100

print "Loaded dicts"