#coding:utf-8
import codecs
from sklearn.externals import joblib

# brand_dic = joblib.load('/Users/martin/workspace/sample_title_data/dic/brand_dic.dump')
brand_dic = joblib.load('/Users/martin/workspace/datalabs/nlp/dict/brand.dict')
fnm = 'eval/naver_goods_name_refined.dat'


def build_seller_dic(fnm):
    dic = {}
    with codecs.open(fnm, 'rt') as fin:
        cnt = 0
        for line in fin:
            seller = line.split(' ')[0].strip()

            if seller in dic:
                dic[seller] += 1
            else :
                dic[seller] = 1
            cnt += 1
            if cnt%10000 == 0:
                print cnt

    joblib.dump(dic, 'dic/seller.dict')


# build_seller_dic(fnm)

seller_dic = joblib.load('dic/seller.dict')

print seller_dic['바보사랑']
print seller_dic['사랑']
print seller_dic['스타']