# -*- coding: utf-8 -*-
from test_segmentor import segment as segment_by_tok
from tr_utils import dic_dump
from common import replace_whitespaces, encode, decode
import codecs
import tokenize_utils as tu

from mining.nlp.SegmentTagger import SegmentTagger
segmentor = SegmentTagger(model_file='/Users/martin/workspace/tokenizer/eval/crf_model/all_feat0_seg_noseg')
def crf_segment(word):
    return segmentor.segment(word)


def segment(txt):
    if type(txt) == unicode:
        line = txt.encode('utf8')

    transformed_line = ""
    for tok in txt.replace('\t',' ').split(' '):
        try:
            transformed_tok = segment_by_tok(tok.strip())
        except:
            print "error at:", tok
            transformed_tok = tok

        if type(transformed_tok) == unicode:
            transformed_tok = transformed_tok.encode('utf8')
        # print tok, transformed_tok
        transformed_line += transformed_tok
        transformed_line += " "
    transformed_line = replace_whitespaces(transformed_line)
    return transformed_line.strip()

def segment2(line):
    if type(line) == unicode:
        line = line.encode('utf8')

    line = tu.replace_specific_token(line)
    line = tu.replace_symbols(line)
    line = tu.tokenize_percent(line)
    line = tu.tokenize_mixed_word(line)
    line = tu.tokenize_mixed_word2(line)
    line = tu.tokenize_chinese(line)
    line = tu.tokenize_dashed_word(line)
    line = tu.replace_whitespaces(line)
    newline = ''
    for tok in line.split(' '):
        newline += tu.tokenize_size_spec(tok)
        newline += ' '
        # line = tu.tokenize_size_spec(line)
    newline = newline.strip()
    return segment(newline)

def segment_file(src_file, dest_file):
    with codecs.open(dest_file, 'w') as d, codecs.open(src_file, 'rt') as f:
        cnt = 0
        for line in f:
            line = line.replace('\n','')
            # transformed_line = crf_segment(line)
            transformed_line = segment2(line)
            d.write(encode(transformed_line) + "\n")

            # print line
            # print transformed_line

            cnt += 1
            if cnt%100 == 0:
                print cnt
            # if cnt == 1000:
            #     break
        dic_dump()

# src_file = 'eval/pre_naver_goods.dat'
# dest_file = 'eval/naver_goods_name_refined.dat'

# src_file = 'eval/eval_test.txt'
# dest_file = 'eval/eval_test_test.txt'
#
# segment_file(src_file, dest_file)
# # print segment("롯데카드 5 % 아이슈즈 멜로디 07-1 11.0 cm 토오픈 펌프스 킬힐")
#
# from eval.eval_tokenizer import evaluate
# evaluate(test=dest_file)