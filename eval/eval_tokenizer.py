# -*- coding: utf-8 -*-
import codecs
from tokenizer.tokenize_utils import replace_whitespaces

PARENT = '/Users/martin/workspace/tokenizer/eval/'
test_file = PARENT + 'eval_test.txt'
gt_file = PARENT + 'gt.txt'
def evaluate(test=test_file, gt=gt_file):
    def contains(tok, toks):
        for t in toks:
            if t.lower() == tok.lower():
                return True
        return False

    with codecs.open(test) as t, codecs.open(gt) as gt:
        t_lines = t.readlines()
        gt_lines = gt.readlines()

        cnt_test = 0
        cnt_gt = 0
        cnt_tp = 0

        for i in range(len(gt_lines)):
            t_line = replace_whitespaces(t_lines[i].replace('\n', ''))
            gt_line = replace_whitespaces(gt_lines[i].replace('\n',''))
            t_toks = t_line.split(' ')
            gt_toks = gt_line.split(' ')

            cnt_gt += len(gt_toks)
            cnt_test += len(t_toks)

            for t_tok in t_toks:
                if contains(t_tok, gt_toks):
                    cnt_tp += 1

        # print cnt_test
        # print cnt_gt

        print "Precision:", float(cnt_tp) / cnt_test
        print "Recall:", float(cnt_tp)/cnt_gt


# evaluate()

