# coding: utf-8

# In[1]:

from datalabs.ryan.buzzni.apis.nlp_api import segment as regacy_segment
from Tokenizer import Tokenizer
import codecs

t = Tokenizer()

# In[6]:

import re
def replace_whitespaces(txt):
    s_token = re.compile('\s+')
    return s_token.sub(' ', txt)
def replace_symbols(txt):
    if type(txt)==str:
        txt = txt.decode('utf8')
    p_token = re.compile(u'(?P<split>[^a-zA-Z0-9가-힣\s])')
    tokenized_str = p_token.sub(' ', txt)
    tokenized_list = [ token for token in tokenized_str.split(" ")]
    return replace_whitespaces(" ".join(tokenized_list).strip())


# In[8]:

fname1 = "/Users/martin/workspace/tokenizer/eval/data/justin_test.dat"
# fname1 = "/Users/martin/workspace/tokenizer/eval/data/test_gt_sampled.txt"


# In[9]:

x_text_list = []
y_list = []
img_list = []
idx = 0
with file(fname1) as fin:
    for line in fin.xreadlines():
#         print(line.strip())
        did, img, cate, name = line.strip().split(" | ")
        x_text_list.append(name)
        y_list.append(cate)
        img_list.append(img)

        idx += 1 
        if idx > 100000:
            break

fname_out = "/Users/martin/workspace/tokenizer/eval/data/test_gt_refined.txt"
with codecs.open(fname_out, 'w') as fout:
    for x in x_text_list:
        if len(x.decode('utf8')) < 50 : continue
        fout.write(x + "\n")



# # In[10]:
#
# from collections import Counter
#
#
# # In[11]:
#
# target_cate_set = set()
# for each in Counter(y_list).most_common(10000):
#     if each[1] > 400:
#         target_cate_set.add(each[0])
# #     print (each[0],each[1])
#
#
# # In[12]:
#
# print (len(target_cate_set))
#
#
# # In[13]:
#
# from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
# from sklearn.linear_model import  LogisticRegression
# from sklearn.model_selection import train_test_split
#
#
# # In[14]:
#
# from sklearn.feature_selection import SelectKBest
# from sklearn.feature_selection import chi2
#
# # In[18]:
# from konlpy.tag import Twitter
# twitter = Twitter()
# def nlp(data):
#     # if type(data)!=unicode:
#     #     data = data.decode('utf-8')
# #     return tSeg.segment(data)[0]
# #     return segmentor.segment(data)
# #     return segmentor.segment(data)+" "+" ".join(twitter.morphs(data))
# #     return " ".join(twitter.morphs(data))
# #     try :
#         data = replace_symbols(data).encode('utf8')
#         return t.tokenize(data)
#
#         # return segmentor.segment(data)
#         # return regacy_segment(data)
#         # data = replace_symbols(data)
#
#         # return " ".join(twitter.morphs(data))
#     # except Error:
#         print "error at:", data
#         return data
#     #return segment(data.encode('utf-8')).decode('utf-8')
#     #return segment(data.encode('utf-8')).decode('utf-8')+ " "+  " ".join(twitter.morphs(data))
#
# print(nlp(u'NEW 솜털 점토세트 솜털점토세트 컬러점토 솜털점토'))
#
#
# # In[19]:
# x_text_list2 = []
# y_list2 = []
#
# lenght_list = []
# word_lenght_list = []
#
# cate_ct_dict = {}
# count = 0
# for x, y in zip(x_text_list, y_list):
#     # if y not in target_cate_set:
#     #     continue
#     # if not y in cate_ct_dict:
#     #     cate_ct_dict[y] = 0
#     # if cate_ct_dict[y] > 400:
#     #     continue
#
#     count += 1
#     # cate_ct_dict[y]+=1
#     x_text_list2.append(nlp(x))
#
#     y_list2.append(y)
#
#     if count % 100 == 0:
#         print count
#
#
#
# # In[20]:
# t.update()
# t_list2 = []
#
# # from sklearn.externals import joblib
# # try:
# #     joblib.dump(x_text_list2, '/Users/martin/workspace/tokenizer/eval/data/justin_x_train.bkk.dump')
# #     joblib.dump(y_list2, '/Users/martin/workspace/tokenizer/eval/data/justin_y_train.bkk.dump')
# # except:
# #     print "dump error"
#
#
# for x, y, img in zip(x_text_list, y_list,img_list):
#     t_list2.append(y.split(";")[0])
# for each in Counter(t_list2).most_common(20):
#     print (each[0],each[1])
#
#
# # In[21]:
#
# # vectorizer = CountVectorizer()
# # vectorizer = CountVectorizer(token_pattern=r"(?u)\b\w+\b", ngram_range=(1, 3),min_df=3)
# vectorizer = CountVectorizer(token_pattern=r"(?u)\b\w+\b",min_df=3)
# # vectorizer = TfidfVectorizer( ngram_range=(1, 3))
#
#
# # In[22]:
#
# print(len(x_text_list2))
#
#
# # In[1]:
#
# x_feature_list = vectorizer.fit_transform(x_text_list2)
#
#
# # In[ ]:
#
# kbest = SelectKBest(chi2, k=int(len(vectorizer.vocabulary_) * 0.95))
# kbest.fit(x_feature_list, y_list2)
# x_feature_list_select = kbest.transform(x_feature_list)
#
#
# # In[ ]:
#
# from scipy import sparse
#
#
# # In[ ]:
#
# # x_feature_list_select2= sparse.hstack((x_feature_list_select, img_feature_list2), format='csr')
#
#
# # In[ ]:
#
# # print x_feature_list_select.shape
#
#
# # In[ ]:
#
# X_train, X_test, y_train, y_test = train_test_split(x_feature_list_select, y_list2, test_size=0.7, random_state=42)
#
#
# # In[ ]:
#
# clf = LogisticRegression(C=1.38949)
# clf.fit(X_train, y_train)
# print(clf.score(X_test, y_test))
#
#
# # In[ ]:
#
# # print(clf.score(X_test, y_test))
#
#
# # In[176]:
#
# # for each in Counter(y_list2).most_common(10):
# #     print each[0],each[1]
#
#
# # In[ ]:
#
# # word
#
# # 0.918608678442 - Twitter
# # 0.904457968433 - 방금 마틴이 준 모델
# # 0.912770273613 - 틴틴 Trie
# # 0.78556231755 - 기존 형태소 분석기
#
#
# # In[ ]:
#
# # unigram - trigram
# # 0.909257335115 - 방금 마틴이 준 모델
# # 0.786205531641 - 기존 형태소 분석기
# # 0.917173816239 - 틴틴 Trie
# # 0.92612933551 - Twitter
#
#
# # In[ ]:
#
# # 0.903604358759 - 기존 + twitter
# # 0.708298407376 - 기존
# # 0.862252025706 - twitter
# # 0.822017323275 - segmentor
# # 0.864487286952  - segmentor + twitter
# # 0.843252305113 - tintin

