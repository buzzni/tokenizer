# -*- coding: utf-8 -*-
from sklearn.externals import joblib
from common import encode
from subprocess import Popen, PIPE
from config import ENV_PATH

PARENT = ENV_PATH + 'dic/'
COMMAND_NODE = '/usr/local/bin/node'
SCRIPT_TR = ENV_PATH + '/test_translate.js'

#TODO: 번역데이터 수집하기
# path_to_en_dic = PARENT + "to_en_dic_dev_test.pkl"
# path_to_ko_dic = PARENT + "to_ko_dic_dev_test.pkl"
path_to_en_dic = PARENT + "to_en.dict"
path_to_ko_dic = PARENT + "to_ko.dict"

TR_EN = joblib.load(path_to_en_dic)
TR_KO = joblib.load(path_to_ko_dic)

def dic_dump():
    joblib.dump(TR_EN, path_to_en_dic)
    joblib.dump(TR_KO, path_to_ko_dic)


def translate(word, debug=False):
    key = encode(word)

    if TR_EN.has_key(key):
        key1 = key
        key2 = TR_EN[key1]
        return TR_EN[key1], TR_KO[key2]
    else :
        if debug : print "need to translate:", key
        tr_result = _translate(key).strip()
        # print tr_result
        if len(tr_result) == 0:
            return None, None

        to_en, to_ko = tr_result.split("|")
        to_en = to_en.strip()
        to_ko = to_ko.strip()

        if len(to_en) == 0 or len(to_ko) == 0:
            return None, None

        key2 = to_en
        TR_EN[key] = to_en
        if not TR_KO.has_key(key2):
            TR_KO.setdefault(key2, [])
        TR_KO[key2].append((key, to_ko))
        return to_en, to_ko

def _translate(word):
    cmd = ' '.join([COMMAND_NODE, SCRIPT_TR, word])
    # print cmd
    p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    output, err = p.communicate()
    rc = p.returncode
    return output

# print _translate('블랜딩')