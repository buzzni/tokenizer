
//npm install --save google-translate-api
const translate = require('google-translate-api');

process.argv.forEach(function (val, index, array) {
	if(index<2) return;

	translate(val, {fron:'ko', to: 'en'}).then(res => {
		process.stdout.write(res.text);
	    //console.log(res.text);
		translate(res.text, {from:'en', to: 'ko'}).then(res => {
			process.stdout.write('|');
			process.stdout.write(res.text);
		}).catch(err=>{
			process.stderr.write(err);
		})

	}).catch(err => {
		process.stderr.write(err);
	    //console.error(err);
	});
});