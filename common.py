# -*- coding: utf-8 -*-
import re
def encode(txt):
    return txt.encode('utf8') if type(txt) == unicode else txt

def decode(txt):
    return txt.decode('utf8') if type(txt) == str else txt

def replace_whitespaces(txt):
    s_token = re.compile('\s+')
    return s_token.sub(' ', txt)

#
# txt = "[현대오피스]::RPT-1300:스텐드 + 봉걸이 포함::트리머형재단기"
# txt = txt.encode('utf8') if type(txt) == unicode else txt
# p_token = re.compile('(?P<split>[^a-zA-Z0-9가-힣\s\.\-])')
# tokenized_str = p_token.sub(' ', txt)
# tokenized_list = [ token for token in tokenized_str.split(" ")]
# newtxt = replace_whitespaces(" ".join(tokenized_list).strip())
# print type(newtxt)